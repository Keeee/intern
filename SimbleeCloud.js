/*
 * Copyright (c) 2015 RF Digital Corp. All Rights Reserved.
 *
 * The source code contained in this file and all intellectual property embodied in
 * or covering the source code is the property of RF Digital Corp. or its licensors.
 * Your right to use this source code and intellectual property is non-transferable,
 * non-sub licensable, revocable, and subject to terms and conditions of the
 * SIMBLEE SOFTWARE LICENSE AGREEMENT.
 * http://www.simblee.com/licenses/SimbleeSoftwareLicenseAgreement.txt
 *
 * THE SOURCE CODE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND.
 *
 * This heading must NOT be removed from this file.
 */

var SimbleeCloud = function (esn, userid) {
  this.myESN = esn || 0x00000000;
  this.userID = userid || 0x00000000;

  this.assignedESN = null;

  this.active = false;

  this.onactive = null;
  this.oninactive = null;
  this.oninitial = null;
  this.onrequest = null;
  this.oncountresult = null;
};

SimbleeCloud.prototype = {};

SimbleeCloud.MAX_MODULE_PAYLOAD = 180;

SimbleeCloud.LITTLE_ENDIAN = true;

SimbleeCloud.MCS_FUNCTION_CALL = 0xffffffff;

SimbleeCloud.MCS_FUNCTION_NONE = 0;
SimbleeCloud.MCS_FUNCTION_PING = 1;
SimbleeCloud.MCS_FUNCTION_PONG = 2;
SimbleeCloud.MCS_FUNCTION_COUNT = 3;
SimbleeCloud.MCS_FUNCTION_INITIAL = 4;

/*
SimbleeCloud.prototype._var = false;
Object.defineProperty(SimbleeCloud.prototype, 'var',
  { get: function() {
      return this._var;
    },
    set: function(v) {
      this._var = v;
    } });
*/

//
SimbleeCloud.prototype.copyBytes = function (target, source, targetOffset, sourceOffset, length) {
  targetOffset = targetOffset || 0;
  sourceOffset = sourceOffset || 0;
  length = length || source.byteLength;

  //最終的には
  var view = new Uint8Array(target, targetOffset);
  view.set(new Uint8Array(source, sourceOffset, length));
};

SimbleeCloud.prototype.send = function (destESN, payload) {
  var len = payload.byteLength;

  if (len > SimbleeCloud.MAX_MODULE_PAYLOAD)
    len = SimbleeCloud.MAX_MODULE_PAYLOAD;

  var msg_len = 1 + 4 + len;

  var buffer = new ArrayBuffer(1 + 4 + len);
  var view = new DataView(buffer);

  //view=0xLLDDDDDDDD L=length of data, D=destination
  view.setUint8(0, 1 + 4 + len);
  view.setUint32(1, destESN, SimbleeCloud.LITTLE_ENDIAN);
  this.copyBytes(buffer, payload, 5, 0, len);

  this.websocket.send(buffer);
};

SimbleeCloud.prototype.open = function () {
  // "this" is the websocket, not the SimbleeCloud object
  var self = this.outer;

  // start byte
  self.websocket.send("!");

  // start message (my esn, then user id for authorization)
  var payload = new ArrayBuffer(4);
  var view = new DataView(payload);
  view.setUint32(0, self.userID, SimbleeCloud.LITTLE_ENDIAN);
  self.send(self.myESN, payload);
};

SimbleeCloud.prototype.close = function () {
  // "this" is the websocket, not the SimbleeCloud object
  var self = this.outer;

  if (self.active) {
    self.active = false;
    if (self.oninactive)
      self.oninactive();
  }
};

SimbleeCloud.prototype.connect = function () {
  try {
    var WebSocket = require('ws');
    this.websocket = new WebSocket("wss://connect.simbleecloud.com:443");

    this.websocket.outer = this;

    this.websocket.on('open', this.open);
    this.websocket.on('message', function message(data, flags) {
      // "this" is the websocket, not the SimbleeCloud object

      //var len = data.slice(0, 1);
      //var originESN = data.slice(1,5);
      //var recData = data.slice(5);

      //console.log(recData.toString());
    });
    //this.websocket.onerror = this.onwebsocketerror;
    this.websocket.on('close', this.close);
  }
  catch (e) {
    console.log(e);
  }
};

module.exports = SimbleeCloud;

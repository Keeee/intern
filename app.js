// Copyright 2015-2016, Google, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// [START app]
//'use strict';

const fs = require('fs');
var SimbleeCloud = require('./SimbleeCloud.js');
var cloud = new SimbleeCloud();

cloud.myESN = 0x00000001; // enter your pool ESN here
cloud.userID = 0xa592afe01; // enter your user id here

cloud.connect();

var cfg = {
ssl: true,
port: 3000,
ssl_key: './../ssl/privkey1.pem',
ssl_cert: './../ssl/fullchain1.pem',
ssl_ca: './../ssl/chain1.pem'
};

var options = {
key: fs.readFileSync(cfg.ssl_key),
cert: fs.readFileSync(cfg.ssl_cert),
ca: fs.readFileSync(cfg.ssl_ca)
}



//app.js
var WebSocketServer = require('ws').Server
	, https = require('https')
	, express = require('express')
	, app = express();

app.use(express.static(__dirname + '/'));
var server = https.createServer(options,app);
var wss = new WebSocketServer({ server: server });
var jsonObj ={};
var browserConnections = [];
var simbleeConnections = [];
var messages = [];

wss.on('connection', function (ws) {
    browserConnections.push(ws);

    ws.on('close', function () {
		console.log("disconnect:" + ws);
		browserConnections = browserConnections.filter(function (conn, i) {
			return (conn === ws) ? false : true;
		});
    });
    ws.on('message', function (message) {
		console.log('Browser Message:', message);
		messages.push(message);
		broadcast(JSON.stringify(message));
    });
});

cloud.websocket.on('message', function message(data, flags) {

	var len = data.slice(0, 1);
	var srcESN = data.slice(1, 5);
	var recData = data.slice(5);
	if (srcESN.readUInt32LE(0) == this.outer.myESN) {
		console.log("This message is by myself!!");
		return
	}
    jsonObj = JSON.parse(recData.toString());
    if(jsonObj.sensor == "Carrot") {
        jsonObj.data = jsonObj.data/200*100;
        if(jsonObj.data>100) jsonObj.data = 100;
        jsonObj.collor = "#ffa500";
    }
    else if(jsonObj.sensor == "Cabbage") {
        jsonObj.data = jsonObj.data/300*100;
        if(jsonObj.data>100) jsonObj.data = 100;
        jsonObj.collor = "#adff2f";
    }
    else if(jsonObj.sensor == "Lettuce") {
        jsonObj.data = jsonObj.data/500*100;
        if(jsonObj.data>100) jsonObj.data = 100;
        jsonObj.collor = "#90ee90";
    }
    else if(jsonObj.sensor == "Onion") {
        jsonObj.data = jsonObj.data/600*100;
        if(jsonObj.data>100) jsonObj.data = 100;
        jsonObj.collor = "#cd853f"
    }
    else if(jsonObj.sensor == "Potato") {
        jsonObj.data = jsonObj.data/800*100;
        if(jsonObj.data>100) jsonObj.data = 100;
        jsonObj.collor = "#f0e68c"
    }
    else if(jsonObj.sensor == "Sprout") {
        jsonObj.data = jsonObj.data/900*100;
        if(jsonObj.data>100) jsonObj.data = 100;
        jsonObj.collor = "#e9967a"
    }
    else if(jsonObj.sensor == "Tomato") {
       jsonObj.data = jsonObj.data/1000*100;
       if(jsonObj.data>100) jsonObj.data = 100;
       jsonObj.collor = "#ff0000"
    }
    else if(jsonObj.sensor == "Mushroom") {
       jsonObj.data = jsonObj.data/1000*100;
       if(jsonObj.data>100) jsonObj.data = 100;
       jsonObj.collor = "#f4a460"
    }
    broadcast(JSON.stringify(jsonObj).toString());
});

function broadcast(message) {
	browserConnections.forEach(function (con, i) {
		con.send(message);
	});
};

//server.listen(3000);
server.listen(cfg.port);
